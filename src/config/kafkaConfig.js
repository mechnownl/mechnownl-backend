require('dotenv').config();
const { Kafka } = require('kafkajs');

console.log('Kafka Broker:', process.env.KAFKA_BROKER);

const kafka = new Kafka({
  clientId: 'mechnownl-backend',
  brokers: [process.env.KAFKA_BROKER],
  ssl: false,
});

module.exports = kafka;

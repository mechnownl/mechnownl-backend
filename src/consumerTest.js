const kafka = require('./config/kafkaConfig');

const consumerTest = async () => {
  const consumer = kafka.consumer({ groupId: 'mechanic-group' });
  await consumer.connect();
  await consumer.subscribe({ topic: 'maintenance_requests', fromBeginning: true });

  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      console.log({
        partition,
        offset: message.offset,
        value: message.value.toString(),
      });
    },
  });
};

consumerTest().catch(console.error);

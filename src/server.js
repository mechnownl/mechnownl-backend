require('dotenv').config();
const app = require('./app');
const { startProducer } = require('./services/kafkaProducer');
const runConsumer = require('./services/kafkaConsumer');

const PORT = process.env.PORT || 3000;
const HOST = process.env.HOST || '0.0.0.0';

app.listen(PORT, HOST, async () => {
  await startProducer();
  await runConsumer();
  console.log(`Server is running on ${HOST}:${PORT}`);
});

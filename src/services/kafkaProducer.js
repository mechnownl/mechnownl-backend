const kafka = require('../config/kafkaConfig');

const producer = kafka.producer();

const startProducer = async () => {
  await producer.connect();
};

const sendMessage = async (topic, message) => {
  await producer.send({
    topic,
    messages: [{ value: JSON.stringify(message) }],
  });
};

module.exports = { startProducer, sendMessage };
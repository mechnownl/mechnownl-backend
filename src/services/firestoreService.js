// src/services/firestoreService.js
const { db } = require('../config/firebaseConfig');
const moment = require('moment-timezone');

const saveRequest = async (userId, request) => {
  const timestamp = moment().tz('America/St_Johns');
  const date = timestamp.format('YYYY-MM-DD'); // Format date as YYYY-MM-DD
  const data = {
    ...request,
    userId,
    date,
    createdAt: timestamp.toISOString()
  };
  const docRef = db.collection('requests').doc(); // Auto-generated document ID
  await docRef.set(data);
  return docRef.id;
};

const getMechanicTokensByType = async (requestType) => {
  const usersRef = db.collection('users');
  const snapshot = await usersRef.where('role', '==', requestType).get();
  if (snapshot.empty) {
    console.log(`No ${requestType}s found.`);
    return [];
  }

  const tokens = [];
  snapshot.forEach(doc => {
    const data = doc.data();
    if (data.deviceToken) {
      tokens.push(data.deviceToken);
    }
  });

  return tokens;
};

const getMechanicTokenById = async (id) => {
  const doc = await db.collection('users').doc(id).get();
  if (!doc.exists) {
    console.log(`No mechanic found with ID: ${id}`);
    return null;
  }
  const data = doc.data();
  return data.deviceToken;
};

const checkIdExists = async (id) => {
  const doc = await db.collection('users').doc(id).get();
  return doc.exists;
};

const getOpenRequestsByType = async (requestType) => {
  const openRequests = [];
  const snapshot = await db.collection('requests')
    .where('status', '==', 'open')
    .where('requestType', '==', requestType)
    .get();

  snapshot.forEach(doc => {
    openRequests.push({ id: doc.id, ...doc.data() });
  });

  return openRequests;
};

module.exports = { saveRequest, getMechanicTokensByType, getMechanicTokenById, checkIdExists, getOpenRequestsByType };

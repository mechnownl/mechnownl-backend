const kafka = require('../config/kafkaConfig');
const { getMechanicTokensByType, getMechanicTokenById } = require('./firestoreService');
const { sendNotification, sendAcceptNotification } = require('./notificationService');

const consumer = kafka.consumer({ groupId: 'mechanic-group' });

const runConsumer = async () => {
  await consumer.connect();
  await consumer.subscribe({ topics: ['maintenance_requests', 'request_accept'], fromBeginning: true });

  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      const request = JSON.parse(message.value.toString());

      if (topic === 'maintenance_requests') {
        const requestType = request.requestType;
        const mechanicTokens = await getMechanicTokensByType(requestType);

        if (mechanicTokens.length === 0) {
          console.log(`No device tokens found for requestType: ${requestType}`);
          return;
        }

        mechanicTokens.forEach(token => {
          sendNotification(token, request);
        });
      } else if (topic === 'request_accept') {
        const hiredMechanic = request.hiredMechanic;
        const mechanicToken = await getMechanicTokenById(hiredMechanic);

        if (!mechanicToken) {
          console.log(`No device token found for mechanicId: ${hiredMechanic}`);
          return;
        }

        sendAcceptNotification(mechanicToken, request);
      }
    },
  });
};

module.exports = runConsumer;

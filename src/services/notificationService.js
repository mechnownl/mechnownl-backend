const admin = require('firebase-admin');

const sendNotification = (deviceToken, message) => {
  const notificationMessage = {
    notification: {
      title: `New Maintenance Request for Issue ${message.problemType}`,
      body: `${message.name} is requesting assistance near you for issue ${message.problemType}`
    },
    token: deviceToken
  };

  admin.messaging().send(notificationMessage)
    .then(response => {
      console.log('Successfully sent message:', response);
    })
    .catch(error => {
      console.error('Error sending message:', error);
    });
};

const sendAcceptNotification = (deviceToken, message) => {
    const notificationMessage = {
      notification: {
        title: `Congratulations!! You are hired`,
        body: `${message.name} has hired you!`
      },
      token: deviceToken
    };

    admin.messaging().send(notificationMessage)
      .then(response => {
        console.log('Successfully sent message:', response);
      })
      .catch(error => {
        console.error('Error sending message:', error);
      });
  };

module.exports = { sendNotification, sendAcceptNotification};

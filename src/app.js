// src/app.js
const express = require('express');
const bodyParser = require('body-parser');
const { createRequest } = require('./controllers/maintenanceController');
const { acceptRequest } = require('./controllers/acceptRequestController');
const { fetchOpenRequests } = require('./controllers/getOpenRequestsController');

const app = express();
app.use(bodyParser.json());

app.post('/api/request', createRequest);
app.post('/api/request/accept', acceptRequest);
app.post('/api/requests/open', fetchOpenRequests);

module.exports = app;

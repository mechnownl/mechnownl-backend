const { sendMessage } = require('../services/kafkaProducer');
const { checkIdExists } = require('../services/firestoreService');

const acceptRequest = async (req, res) => {
  const { name, hiredMechanic } = req.body;

  if (!name || !hiredMechanic) {
    return res.status(400).send('Invalid request: all fields are required');
  }

  try {
    const idExists = await checkIdExists(hiredMechanic);
    if (!idExists) {
      return res.status(403).send('ID not found');
    }

    const kafkaMessage = {
      name,
      hiredMechanic
    };

    await sendMessage('request_accept', kafkaMessage);

    res.status(200).send('Request acceptance submitted successfully');
  } catch (error) {
    console.error('Error handling request acceptance', error);
    res.status(500).send('Error submitting request acceptance');
  }
};

module.exports = { acceptRequest };

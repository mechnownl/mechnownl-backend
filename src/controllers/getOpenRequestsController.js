// src/controllers/getOpenRequestsController.js
const { getOpenRequestsByType } = require('../services/firestoreService');

const fetchOpenRequests = async (req, res) => {
  const { requestType } = req.body;

  // Validate input data
  if (!requestType) {
    return res.status(400).send('Invalid request: requestType is required');
  }

  // Validate requestType
  const validRequestTypes = [
    'automobileMechanic', 'locksmith', 'plumber', 'roofer', 'electrician', 'heaterMechanic', 'carpenter'
  ];
  if (!validRequestTypes.includes(requestType)) {
    return res.status(400).send('Invalid requestType');
  }

  try {
    const openRequests = await getOpenRequestsByType(requestType);
    res.status(200).json(openRequests);
  } catch (error) {
    console.error('Error fetching open requests', error);
    res.status(500).send('Error fetching open requests');
  }
};

module.exports = { fetchOpenRequests };

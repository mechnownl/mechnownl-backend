const { sendMessage } = require('../services/kafkaProducer');
const { checkIdExists, saveRequest } = require('../services/firestoreService');

const createRequest = async (req, res) => {
  const { id, name, userLatLang, requestType, problemType, note } = req.body;

  if (!id || !name || !userLatLang || !requestType || !problemType) {
    return res.status(400).send('Invalid request: all fields except note are required');
  }

  const validRequestTypes = [
    'automobileMechanic', 'locksmith', 'plumber', 'roofer', 'electrician', 'heaterMechanic', 'carpenter'
  ];
  if (!validRequestTypes.includes(requestType)) {
    return res.status(400).send('Invalid requestType');
  }

  try {
    const idExists = await checkIdExists(id);
    if (!idExists) {
      return res.status(403).send('ID not found');
    }

    const kafkaMessage = {
      id,
      name,
      userLatLang,
      requestType,
      problemType,
      note: note || '',
      status: 'open'
    };

    await sendMessage('maintenance_requests', kafkaMessage);
    const req_id = await saveRequest(id, kafkaMessage);
    res.status(200).send(req_id);
  } catch (error) {
    console.error('Error handling request', error);
    res.status(500).send('Error submitting request');
  }
};

module.exports = { createRequest };

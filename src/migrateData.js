// src/migrateData.js
const { db } = require('./config/firebaseConfig');
const moment = require('moment-timezone');

const migrateData = async () => {
  const usersSnapshot = await db.collection('requests').get();

  usersSnapshot.forEach(async (userDoc) => {
    const userId = userDoc.id;
    const dateCollections = await db.collection('requests').doc(userId).listCollections();

    for (const dateCollection of dateCollections) {
      const date = dateCollection.id;
      const requestsSnapshot = await dateCollection.get();

      requestsSnapshot.forEach(async (requestDoc) => {
        const data = requestDoc.data();
        const timestamp = moment().tz('America/St_Johns');
        const newDocRef = db.collection('requests').doc();

        await newDocRef.set({
          ...data,
          userId,
          date,
          createdAt: timestamp.toISOString()
        });

        console.log(`Migrated request ${requestDoc.id} from ${userId}/${date}`);
      });
    }
  });

  console.log('Data migration completed');
};

migrateData().catch(console.error);

require('dotenv').config();
const kafka = require('./config/kafkaConfig');

const producerTest = async () => {
  const producer = kafka.producer();
  await producer.connect();

  const message = {
    name: 'MD Karimul Hasan',
    email: 'khp4121@gmail.com',
    phone: '7096852032',
    area: 'St. Johns',
  };

  try {
    await producer.send({
      topic: 'maintenance_requests',
      messages: [{ value: JSON.stringify(message) }],
    });
    console.log('Message produced successfully');
  } catch (err) {
    console.error('Error producing message', err);
  } finally {
    await producer.disconnect();
  }
};

producerTest();

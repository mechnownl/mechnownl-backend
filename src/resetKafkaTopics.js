const { Kafka } = require('kafkajs');

const kafka = new Kafka({
  clientId: 'kafka-admin',
  brokers: [process.env.KAFKA_BROKER || 'localhost:9092']
});

const admin = kafka.admin();

const topics = ['maintenance_requests','request_accept'];

const resetTopics = async () => {
  await admin.connect();

  try {
    await admin.deleteTopics({
      topics,
      timeout: 5000
    });
    console.log('Topics deleted successfully');
  } catch (error) {
    console.error('Error deleting topics:', error);
  }

  const topicConfig = topics.map(topic => ({
    topic,
    numPartitions: 1,
    replicationFactor: 1
  }));

  try {
    await admin.createTopics({
      topics: topicConfig,
      validateOnly: false
    });
    console.log('Topics created successfully');
  } catch (error) {
    console.error('Error creating topics:', error);
  }

  await admin.disconnect();
};

resetTopics().catch(console.error);
